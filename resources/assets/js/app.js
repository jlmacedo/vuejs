
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Vue.use(VueRouter);
window.Vue.use(Vuex);

const router = new VueRouter({
    routes,
    mode: 'history',
    transitionOnLoad: true,
    root: '/'
});

router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next('/404');
    } else {
        next();
    }
});

import App from './components/App.vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { routes } from './routes';

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
