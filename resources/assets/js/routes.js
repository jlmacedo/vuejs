import Home from './components/Home.vue';
import Contact from './components/Contact.vue';
import Admin from './components/Admin.vue';
import NotFound from './components/NotFound.vue';

export const routes = [
    { path: '/', component: Home},
    { path: '/contact', component: Contact},
    { path: '/404', component: NotFound, name: '404'},
    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
        /*children: [
            {
                path: 'new',
                name: 'New',
                component: New
            },
            {
                path: '',
                name: 'Products',
                component: Products
            },
            {
                path: 'edit/:id',
                name: 'Edit',
                component: Edit
            }
        ]*/
    },
]